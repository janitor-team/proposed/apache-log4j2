apache-log4j2 (2.17.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.17.2.

 -- Markus Koschany <apo@debian.org>  Wed, 04 May 2022 19:54:52 +0200

apache-log4j2 (2.17.1-1) unstable; urgency=high

  * Team upload.
  * New upstream version 2.17.1.
    - Fix CVE-2021-44832:
      Apache Log4j2 is vulnerable to a remote code execution
      (RCE) attack where an attacker with permission to modify the logging
      configuration file can construct a malicious configuration using a JDBC
      Appender with a data source referencing a JNDI URI which can execute
      remote code. This issue is fixed by limiting JNDI data source names to
      the java protocol.
      Thanks to Salvatore Bonaccorso for the report. (Closes: #1002813)

 -- Markus Koschany <apo@debian.org>  Wed, 29 Dec 2021 11:44:21 +0100

apache-log4j2 (2.17.0-1) unstable; urgency=high

  * Team upload.
  * New upstream version 2.17.0.
    - Fix CVE-2021-45105:
      Apache Log4j2 did not protect from uncontrolled recursion from
      self-referential lookups. When the logging configuration uses a
      non-default Pattern Layout with a Context Lookup (for example,
      $${ctx:loginId}), attackers with control over Thread Context Map (MDC)
      input data can craft malicious input data that contains a recursive
      lookup, resulting in a denial of service. (Closes: #1001891)
      Thanks to Salvatore Bonaccorso for the report.

 -- Markus Koschany <apo@debian.org>  Sat, 18 Dec 2021 17:09:22 +0100

apache-log4j2 (2.16.0-1) unstable; urgency=high

  * Team upload.
  * New upstream version 2.16.0.
    - Fix CVE-2021-45046:
      It was found that the fix to address CVE-2021-44228 in Apache Log4j
      2.15.0 was incomplete in certain non-default configurations. This could
      allow attackers with control over Thread Context Map (MDC) input data
      when the logging configuration uses a non-default Pattern Layout with
      either a Context Lookup (for example, $${ctx:loginId}) or a Thread
      Context Map pattern (%X, %mdc, or %MDC) to craft malicious input data
      using a JNDI Lookup pattern resulting in a denial of service (DOS)
      attack.
      Thanks to Salvatore Bonaccorso for the report. (Closes: #1001729)

 -- Markus Koschany <apo@debian.org>  Wed, 15 Dec 2021 02:38:06 +0100

apache-log4j2 (2.15.0-1) unstable; urgency=high

  * Team upload.
  * New upstream version 2.15.0.
    - Fix CVE-2021-44228:
      Chen Zhaojun of Alibaba Cloud Security Team discovered that JNDI features
      used in configuration, log messages, and parameters do not protect
      against attacker controlled LDAP and other JNDI related endpoints. An
      attacker who can control log messages or log message parameters can
      execute arbitrary code loaded from LDAP servers when message lookup
      substitution is enabled. From version 2.15.0, this behavior has been
      disabled by default. (Closes: #1001478)
  * Update debian/watch to track the latest releases.
  * Declare compliance with Debian Policy 4.6.0.

 -- Markus Koschany <apo@debian.org>  Sat, 11 Dec 2021 15:01:57 +0100

apache-log4j2 (2.13.3-1) unstable; urgency=medium

  * New upstream release
    - Refreshed the patches
    - Ignore the new log4j-docker, log4-jpl, log4j-kubernetes and
      log4j-spring-cloud-config modules
  * Depend on libgeronimo-jpa-2.0-spec-java instead of libjpa-2.1-spec-java
  * Removed the -java-doc package (Closes: #835382)
  * Standards-Version updated to 4.5.1
  * Switch to debhelper level 13
  * No longer track the release candidates

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 19 Jan 2021 14:29:47 +0100

apache-log4j2 (2.11.2-1) unstable; urgency=medium

  * Team upload.

  [ tony mancill ]
  * Revert "Drop support for mongodb (Debian: #919095)"

  [ Emmanuel Bourg ]
  * New upstream release
    - Refreshed the patches
    - Updated the Maven rules
  * Sort the entries in the plugin cache (Log4j2Plugins.dat) to make
    the build reproducible
  * Standards-Version updated to 4.4.0

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 10 Sep 2019 10:32:34 +0200

apache-log4j2 (2.11.1-2) unstable; urgency=medium

  * Team upload.
  * Drop support for mongodb (Closes: #919095)
  * Standards-Version updated to 4.3.0

 -- tony mancill <tmancill@debian.org>  Sat, 12 Jan 2019 11:33:45 -0800

apache-log4j2 (2.11.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Build the new log4j-core-java9 module
    - Build the mongodb3 module and ignore the mongodb2 one
    - Ignore the new log4j-jdbc-dbcp2, log4j-jpa and log4j-slf4j18-impl modules
  * Worked around a javadoc bug in Java 10 causing an IllegalArgumentException
    (Closes: #905139)
  * Standards-Version updated to 4.1.5
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 31 Jul 2018 17:12:58 +0200

apache-log4j2 (2.10.0-2) unstable; urgency=medium

  * Team upload.
  * Generate code usable with the Java 8 API to help with the transition
  * Standards-Version updated to 4.1.4

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 06 Apr 2018 09:14:54 +0200

apache-log4j2 (2.10.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Build the new log4j-api-java9 module
    - Ignore the new log4j-appserver module
    - Replaced the log4j-nosql module with log4j-couchdb and log4j-cassandra
    - Updated the Maven rules
    - New dependency on libjackson2-annotations-java
  * Fixed the build failure with Java 9 (Closes: #893085)
  * Standards-Version updated to 4.1.3
  * Switch to debhelper level 11
  * Removed the Maven wrapper from the upstream tarball

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 16 Mar 2018 17:14:19 +0100

apache-log4j2 (2.8.2-2) unstable; urgency=medium

  * Team upload.
  * Added the missing build dependency on libnetty-java (Closes: #880239)
  * Standards-Version updated to 4.1.1

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 30 Oct 2017 23:14:54 +0100

apache-log4j2 (2.8.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Ignore the new test modules log4j-osgi and log4j-core-its
    - Disabled the Cassandra appender (missing dependencies)
    - Updated the Maven rules
    - Install RELEASE-NOTES.md instead of RELEASE-NOTES.txt

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 21 Jun 2017 12:55:58 +0200

apache-log4j2 (2.7-2) unstable; urgency=medium

  * Team upload.
  * Fixed CVE-2017-5645: When using the TCP socket server or UDP socket server
    to receive serialized log events from another application, a specially
    crafted binary payload can be sent that, when deserialized, can execute
    arbitrary code (Closes: #860489)

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 18 Apr 2017 14:30:00 +0200

apache-log4j2 (2.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Ignore the new log4j-api-scala modules
    - New dependencies on libconversant-disruptor-java, libjcommander-java
      and libjctools-java
  * Transition to the Servlet API 3.1
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 21 Oct 2016 18:22:32 +0200

apache-log4j2 (2.6.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 14 Jul 2016 19:32:56 +0200

apache-log4j2 (2.6.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Fixed the compatibility with jackson and mongodb
    - New dependencies on groovy, libwoodstox-java and libbsh-java
    - Ignore the new test dependencies
  * Exclude the minified JavaScript files from the upstream tarball
  * Standards-Version updated to 3.9.8
  * Use a secure Vcs-Git URL

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 08 Jul 2016 16:08:33 +0200

apache-log4j2 (2.4-2) unstable; urgency=medium

  * Team upload.
  * maven.rules: Fix substitution rules for javax.servlet API.
    Thanks to Chris Lamb for the report. (Closes: #809619)
  * Switch from cdbs to dh sequencer.
  * Vcs-Browser: Use https.

 -- Markus Koschany <apo@debian.org>  Sat, 09 Jan 2016 14:23:29 +0100

apache-log4j2 (2.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - New dependencies on libcommons-compress-java, libcommons-csv-java
      and libjeromq-java
    - Ignore the new liquibase module
    - Disabled the new Kafka appender

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 22 Oct 2015 19:44:48 +0200

apache-log4j2 (2.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * liblog4j2-java.poms:
    - Add and enable the new modules: log4j-nosql, log4j-web, log4j2-jul
      and log4j-bom
    - Remove the log4j-osgi module
    - Ignore log4j-iostreams and log4j-perf modules
  * maven.ignoreRules: Ignore all artifacts which make the build FTBFS,
    including maven-failsafe-plugin, woodstox-core-asl, json-unit,
    activemq-broker.
  * debian/control:
    - Declare compliance with Debian Policy 3.9.6.
    - Switch Vcs-Browser field to cgit.
    - New build dependencies on libmaven-source-plugin-java,
      libcommons-lang3-java, libjackson2-dataformat-yaml,
      libjackson2-dataformat-xml-java and jackson-module-jaxb-annotations
  * Update maven.rules due to additional build-dependencies.

  [ Emmanuel Bourg ]
  * Build depend on libmail-java instead of libgnumail-java
  * debian/watch: Watch the release tags on Github

 -- Markus Koschany <apo@gambaru.de>  Fri, 29 May 2015 14:43:11 +0200

apache-log4j2 (2.0~beta9-1) unstable; urgency=medium

  * Initial release (Closes: #718867)

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 19 Mar 2014 11:49:25 +0100
